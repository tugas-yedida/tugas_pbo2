public class Main {
    public static void main(String[] args) {
        Lampu lampu1 = new Lampu();
        Lampu lampu2 = new Lampu();

        lampu1.nyalakanLampu();
        System.out.println("Lampu 1 Nyala: " + lampu1.nyala);

        lampu2.matikanLampu();
        System.out.println("Lampu 2 Nyala: " + lampu2.nyala);
    }
}