public class SepedaGunung {
    private String merk;
    private int jumlahRoda;
    private int jumlahGear;
    private String direm;
    public SepedaGunung(String merk, int jumlahRoda, int jumlahGear, String direm) {
        this.merk = merk;
        this.jumlahRoda = jumlahRoda;
        this.jumlahGear = jumlahGear;
        this.direm = direm;
    }
    public void SepedaGunung() {
        System.out.println(" Sepeda gunung bermerk " + this.merk + " memiliki jumlah roda " + this.jumlahRoda);
        System.out.println( " Jumlah gear " + this.jumlahGear);
        System.out.println( " Sepeda " + this.direm + ".");
    }
}